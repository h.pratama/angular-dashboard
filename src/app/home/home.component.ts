import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js/auto';

const data = {
  labels: [
    'Red',
    'Blue',
    'Yellow'
  ],
  datasets: [{
    label: 'My First Dataset',
    data: [300, 50, 100],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)'
    ],
    hoverOffset: 4
  }]
};

const doughnut = {
  labels: [
    'Red',
    'Blue',
    'Yellow'
  ],
  datasets: [{
    label: 'My First Dataset',
    data: [300, 50, 100],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)'
    ],
    hoverOffset: 4
  }]
};

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  chart:any
  dougChart:any

  constructor() { }

  ngOnInit(): void {
    this.chart = new Chart('MyChart', {
      type: 'pie', //this denotes tha type of chart
        data: data,
        options: {
          aspectRatio:2.5
        }
    });

    this.dougChart = new Chart('DoughChart', {
      type: 'doughnut',
      data: doughnut,
      options: {
        aspectRatio:2.5
      }
    })
  }

}
