import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  router:Router

  currentPath = '/'

  constructor(
    routing:Router
  ) {
    this.router = routing
  }

  ngOnInit(): void {
    
  }

  navigate(path:string) {
    this.currentPath = path;
    this.router.navigate([path]);
  }
}
